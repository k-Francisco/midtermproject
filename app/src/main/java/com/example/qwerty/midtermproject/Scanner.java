package com.example.qwerty.midtermproject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * Created by qwerty on 9/3/2016.
 */
public class Scanner extends Activity implements ZXingScannerView.ResultHandler {
    private ZXingScannerView mScanner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mScanner = new ZXingScannerView(this);
        setContentView(mScanner);
        mScanner.setResultHandler(this);
        mScanner.startCamera();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mScanner.stopCamera();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        mScanner.startCamera();
    }

    @Override
    public void handleResult(Result result) {
        String item = result.getText();
        Intent intent = new Intent();
        intent.putExtra("item", item);
        setResult(1, intent);
        finish();
    }
}
