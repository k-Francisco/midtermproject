package com.example.qwerty.midtermproject;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Button mSend;
    private TextView mTotal;
    private ListView mItemList;
    private ArrayList<String> container = new ArrayList<>();
    private ArrayAdapter<String> adapter;
    private double totalPrice = 0;
    private String items = "";
    private String contactNumber = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();

        init();

    }

    private void init() {
        mSend = (Button) findViewById(R.id.btnSend);
        mTotal = (TextView) findViewById(R.id.tvTotal);
        mItemList = (ListView) findViewById(R.id.itemList);
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, container);
        mItemList.setAdapter(adapter);

        mSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (totalPrice == 0)
                    Toast.makeText(MainActivity.this, "No items to be sent", Toast.LENGTH_SHORT).show();
                else {
                    items += "Total: " + String.valueOf(totalPrice);
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);
                    startActivityForResult(intent, 2);
                }
            }
        });


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setImageResource(R.drawable.plus);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scannerActivity();
            }
        });
    }

    private void scannerActivity() {
        Intent intent = new Intent(this, Scanner.class);
        startActivityForResult(intent, 1);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String[] parse;
        String tbp;

        if(requestCode == 1) {
            if (data != null) {
                tbp = data.getStringExtra("item");
                parse = tbp.split("\\|\\|\\|");

                if (parse.length != 3)
                    Toast.makeText(this, "QR code format not accepted!", Toast.LENGTH_SHORT).show();
                else if (parse.length == 3) {
                    try {
                        totalPrice += Double.parseDouble(parse[2]) * Double.parseDouble(parse[1]);
                        String temp = "Item name: " + parse[0] + "\n" + "Quantity: " + parse[1] + "\n" + "Price: " + parse[2];
                        container.add(temp);
                        adapter.notifyDataSetChanged();
                        items += temp + "\n\n";
                        mTotal.setText(String.valueOf(totalPrice));
                    } catch (Exception q) {
                        Toast.makeText(this, "QR code format not accepted!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
        else if(requestCode == 2){
            if(data != null){
                Uri uri = data.getData();
                if(uri != null){
                    Cursor cursor = null;
                    try{
                     cursor = getContentResolver().query(uri, new String[]{
                             ContactsContract.CommonDataKinds.Phone.NUMBER
                     },null,null,null);
                        if(cursor != null && cursor.moveToFirst()){
                            contactNumber = cursor.getString(0);
                            SmsManager sms = SmsManager.getDefault();
                            sms.sendTextMessage(contactNumber, null, items, null,null);
                        }
                    }
                    catch (Exception e){}
                    finally {
                        if(cursor != null)
                            cursor.close();
                    }
                }
            }

        }
        else{}

    }
}
